1. The solidwork format export into .stl format
2. Open the .stl file with software Cura.
   ![](https://gitlab.com/ottopicbed/bed2/uploads/4512bf0bf26fd00acb2498776dd55153/微信截图_20200818110430.png)
3. adjust the size and angle
   ![](https://gitlab.com/ottopicbed/bed2/uploads/250c6310a1989c0c4930cec148eb2b5f/微信截图_20200818111230.png)
4. Set the printing parameter 
   ![](https://gitlab.com/ottopicbed/bed2/uploads/f7ce4a51891aee4c3aecd47a548fe173/微信截图_20200818111705.png)
   
   Special note: Infill, top and bottom speed low down; Support, check the printing area out of panel; check the original point location
   ![](https://gitlab.com/ottopicbed/bed2/uploads/f02e440fc1b58e3f6d834810300f9a66/微信截图_20200814102351.png)
5. preview and save to file
6. copy in SD, insert the Card
7. Stat the machine, pick the related files, printing.

Finish product
![](https://gitlab.com/ottopicbed/bed2/uploads/b5cb16f822f01e5db1ba0264b65a9186/微信图片_20200818121834.jpg)

